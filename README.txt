README.txt
==========

INTRODUCTION
------------

This is a very simple project that provides a Link field formatter to embed Github Gists.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will provide a Link field formatter that can be used under
entity display management.
